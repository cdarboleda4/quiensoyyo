console.log("*** AdivinaQuinSoy ***");

let personajes = [{
    name: ['mario bross', 'bigoton'],
    foto: 'mario.png',
    preguntas: ['Es animal?', 'Es nitento', 'Tiene bigote ?', 'Usa Armas?', 'Es Viejo?'],
    respuetas: ['no', 'si', 'si', 'no', 'si'],
}, {
    name: ['steve', 'minecraft'],
    foto: 'minecraft.png',
    preguntas: ['Pertenece a una revista?', 'Tiene poderes?', 'Construye Cosas?', 'Es de un video juego?', 'Pertenece a un mundo de cuadros?'],
    respuetas: ['no', 'no', 'si', 'si', 'si'],
}, {
    name: ['goku', 'kakaroto'],
    foto: 'goku.png',
    preguntas: ['Vuela?', 'Usa armadura ?', 'Tiene poderes ?', 'Usa Armas?', 'Es ser humano?'],
    respuetas: ['si', 'no', 'si', 'no', 'no'],
}, {
    name: ['luigi', 'luigi'],
    foto: 'luigi.png',
    preguntas: ['Pertenece a una pelicula?', 'Pertenece a un juego?', 'Corre Demasiado?', 'Tiene Un Hermano?', 'Tiene poderes?'],
    respuetas: ['no', 'si', 'no', 'si', 'no'],
}, {
    name: ['deadpool', 'pool'],
    foto: 'deadpool.png',
    preguntas: ['Pertenece A Una pelicula?', 'Tiene Padres?', 'Es Tranquilo?', 'Es Gracioso?', 'Tiene Buena Imaginación?'],
    respuetas: ['si', 'no', 'no', 'si', 'si'],
}
];

const validarNombre = (nombre, i) => {
    let bandera = false;
    personajes[i].name.forEach(nomPer => {
        if (nombre == nomPer) {
            bandera = true;
        }
    });
    console.log("bandera" + bandera)
    return bandera;
}


const btnJugar = document.getElementById("btnJugar");
const imgPersonaje = document.getElementById("imgPersonaje");
let indice = 0;
let opacidad;
var puntos = 0;
var aciertos = 0;


console.log(indice);

btnJugar.addEventListener('click', () => {

    indice = parseInt(Math.random() * 5);

    const pregunta0 = document.getElementById("pregunta0");
    const pregunta1 = document.getElementById("pregunta1");
    const pregunta2 = document.getElementById("pregunta2");
    const pregunta3 = document.getElementById("pregunta3");
    const pregunta4 = document.getElementById("pregunta4");



  

    imgPersonaje.src = "./assets/images/" + personajes[indice].foto;
    opacidad = 20;
    imgPersonaje.style.filter = "blur(" + opacidad + "px)";

    pregunta0.value = personajes[indice].preguntas[0];
    pregunta1.value = personajes[indice].preguntas[1];
    pregunta2.value = personajes[indice].preguntas[2];
    pregunta3.value = personajes[indice].preguntas[3];
    pregunta4.value = personajes[indice].preguntas[4];
   
    rta0.disabled = false;
    rta1.disabled = false;
    rta2.disabled = false;
    rta3.disabled = false;
    rta4.disabled = false;
    document.getElementById("icoRta0").src = "";
    document.getElementById("icoRta1").src = "";
    document.getElementById("icoRta2").src = "";
    document.getElementById("icoRta3").src = "";
    document.getElementById("icoRta4").src = "";
    rta0.selectedIndex = 0;
    rta1.selectedIndex = 0;
    rta2.selectedIndex = 0;
    rta3.selectedIndex = 0;
    rta4.selectedIndex = 0;


})




const rta0 = document.getElementById("rta0");

rta0.addEventListener('change', () => {

    if (rta0.value == personajes[indice].respuetas[0]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta0").src = "./assets/images/si.png";

        puntos = puntos + 10;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
        aciertos = aciertos + 1;
        document.getElementById('mAciertos').innerHTML = `Número de aciertos:  ${aciertos} `;
    } else {
        document.getElementById("icoRta0").src = "./assets/images/no.png";
        puntos = puntos - 5;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
    }

    rta0.disabled = true;
})

const rta1 = document.getElementById("rta1");

rta1.addEventListener('change', () => {

    if (rta1.value == personajes[indice].respuetas[1]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta1").src = "./assets/images/si.png";
        puntos = puntos + 10;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
        aciertos = aciertos + 1;
        document.getElementById('mAciertos').innerHTML = `Número de aciertos:  ${aciertos} `;
    } else {
        document.getElementById("icoRta1").src = "./assets/images/no.png";
        puntos = puntos - 5;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
    }

    rta1.disabled = true;
})


const rta2 = document.getElementById("rta2");

rta2.addEventListener('change', () => {

    if (rta2.value == personajes[indice].respuetas[2]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta2").src = "./assets/images/si.png";
        puntos = puntos + 10;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
        aciertos = aciertos + 1;
        document.getElementById('mAciertos').innerHTML = `Número de aciertos:  ${aciertos} `;
    } else {
        document.getElementById("icoRta2").src = "./assets/images/no.png";
        puntos = puntos - 5;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
    }

    rta2.disabled = true;
})

const rta3 = document.getElementById("rta3");

rta3.addEventListener('change', () => {

    if (rta3.value == personajes[indice].respuetas[3]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta3").src = "./assets/images/si.png";
        puntos = puntos + 10;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
        aciertos = aciertos + 1;
        document.getElementById('mAciertos').innerHTML = `Número de aciertos:  ${aciertos} `;
    } else {
        document.getElementById("icoRta3").src = "./assets/images/no.png";
        puntos = puntos - 5;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
    }

    rta3.disabled = true;
})

const rta4 = document.getElementById("rta4");

rta4.addEventListener('change', () => {

    if (rta4.value == personajes[indice].respuetas[4]) {
        opacidad = opacidad - 4;
        imgPersonaje.style.filter = "blur(" + opacidad + "px)";
        document.getElementById("icoRta4").src = "./assets/images/si.png";
        puntos = puntos + 10;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
        aciertos = aciertos + 1;
        document.getElementById('mAciertos').innerHTML = `Número de aciertos:  ${aciertos} `;
    } else {
        document.getElementById("icoRta4").src = "./assets/images/no.png";
        puntos = puntos - 5;
        document.getElementById('mPuntos').innerHTML = `Puntos Logrados:  ${puntos}`;
    }

    rta4.disabled = true;
})



const btnRespuesta = document.getElementById("btnRespuesta");

btnRespuesta.addEventListener('click', () => {

    const RespuestaGeneral = document.getElementById("RespuestaGeneral").value

    const rt = RespuestaGeneral.toLowerCase();
    console.log(rt)
    console.log("Respuesta General " + RespuestaGeneral)
    if (validarNombre(rt, indice)) {

        Swal.fire({
            title: 'Felicidades!',
            text: 'Adivinaste el personaje!',
            imageUrl: './assets/images/winner.jpg',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
        })
        console.log(" GANASTE ");
    } else {
        Swal.fire({
            title: 'Perdiste!',
            text: 'Intentalo nuevamente!',
            imageUrl: './assets/pic/gameover.png',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
        })
        console.log(" GAME OVER ")

    }

})
